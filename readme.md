# Scrollfire

Scrollfire is a 3.0 module that can be used to determine when an element scrolls into view, 
and accepts a call back to perform some action based on that element coming into view.

This is great for cases where we need to fire tracking when an element comes into view.

## Example

```js
scrollFire.track(root.find('[data-seo-listing]'), function(index, item) {
    var seoModel = {
        event: "tEvent",
        eventCategory: $(item).attr('data-seo-gtmcat'),
        eventAction: "Scroll Into View",
        eventLabel: $(item).attr('data-seo-title'),
        row: $(item).attr('data-seo-row'),
        globalRow: $(item).attr('data-seo-globalrow'),
        noninteraction: true
    };

    gtmDataLayer.push(seoModel);
});
```

## Instructions
1. Add `custom_scrollfire.js` to the `common -> virtuals -> js` directory.
2. Add an entry in `requireJsPaths` under the `common` key.
3. Within a given template file require the module `"plugins_common_custom_scrollfire"`