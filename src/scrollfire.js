define(["jquery", "lodash", "exports"], function($, lodash, exports) {
	var track = function(items, cb, debounce) {
		var debounce = debounce || 0;
		$(items).each(function(index, item) {
			$(this).on('scrolled-into-view', function(event, data) {
				cb(index, item);
				$(this).off('scrolled-into-view');
			});
		});
		
		var scrollFn = lodash.debounce(function() {
			$(items).each(function(index, item) {
				if (isScrolledIntoView($(this))) {
					$(this).trigger('scrolled-into-view');
				}
			});
		}, debounce);

		$(window).on('scroll', scrollFn);

		function isScrolledIntoView(element) {
			var docViewTop = $(window).scrollTop();
			var docViewBottom = docViewTop + $(window).height();
			var viewportHeight = docViewBottom - docViewTop;
			var elemTop = $(element).offset().top;
			var elemHeight = $(element).height();
			var elemBottom = elemTop + elemHeight;

			if (elemHeight > viewportHeight) {
				/* if element is taller then view port, fire when max availble in view,
					eventually this can be a setting to determine if it should only run when ENTIRELY in view */
				return (((elemTop+viewportHeight-30) <= docViewBottom) && (elemTop >= docViewTop)); // 30 = padding so we have room for item
			} else {
				// if element smaller then viewport, fire when whole element is visible
				return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
			}
		}
	};

	exports.track = track;
});